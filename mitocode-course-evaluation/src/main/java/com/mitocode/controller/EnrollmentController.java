package com.mitocode.controller;

import com.mitocode.model.Course;
import com.mitocode.model.Enrollment;
import com.mitocode.service.IEnrollmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/enrollment")
public class EnrollmentController {

    @Autowired
    private IEnrollmentService service;

    @GetMapping
    public Mono<ResponseEntity<Flux<Enrollment>>> list() {
        Flux<Enrollment> enrollmentFlux = service.listAll();

        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(enrollmentFlux));
    }

    @PostMapping
    public Mono<ResponseEntity<Enrollment>> register(@RequestBody Enrollment enrollment, final ServerHttpRequest request) {
        return service.register(enrollment)
                .map(e -> ResponseEntity.created(URI.create(request.getURI().toString().concat("/").concat(e.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(e)
                );
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Enrollment>> listById(@PathVariable("id") String id) {
        return service.listById(id)
                .map(c -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(c)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> delete(@PathVariable("id") String id) {
        return service.listById(id)
                .flatMap(p -> service.delete(p.getId())
                        .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT))))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<Enrollment>> modify(@Valid @RequestBody Enrollment enrollment) {
        return service.modify(enrollment)
                .map(p -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                );
    }
}
