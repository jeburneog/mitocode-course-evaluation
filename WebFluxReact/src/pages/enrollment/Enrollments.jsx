import React, { useCallback, useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { Breadcrumb, Button, Container, Divider, Grid, Header, Icon, Popup, Segment, Table } from 'semantic-ui-react'
import PropTypes from 'prop-types'

import EnrollmentService from '../../app/api/enrollmentService'
import LoadingComponent from '../../components/common/LoadingComponent'

const Enrollments = ({ history }) => {
  const [enrollments, setEnrollment] = useState([])
  const [loading, setLoading] = useState(false)

  const fetchEnrollments = useCallback(async () => {
    setLoading(true)
    try {
      const enrollments = await EnrollmentService.fetchEnrollments()
      if (enrollments) setEnrollment(enrollments)
      setLoading(false)
    } catch (error) {
      setLoading(false)
      toast.error(error)
    }
  }, [])

  useEffect(() => {
    fetchEnrollments()
  }, [fetchEnrollments])

  let enrollmentsList = <h4>There is no registered enrollments</h4>

  if (enrollments && enrollments.length > 0) {
    enrollmentsList = (
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width="5">Course ID</Table.HeaderCell>
            <Table.HeaderCell width="2">Registered On</Table.HeaderCell>
            <Table.HeaderCell width="3" />
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {enrollments.map((enrollment) => (
            <Table.Row key={enrollment.id}>
              <Table.Cell>{enrollment.id}</Table.Cell>
              <Table.Cell>{new Date(enrollment.createIn).toLocaleDateString()}</Table.Cell>
              <Table.Cell>
                <Popup
                  inverted
                  content="Enrollment Detail"
                  trigger={
                    <Button
                      color="violet"
                      icon="address card outline"
                      onClick={() => {
                        history.push(`/enrollment/${enrollment.id}`)
                      }}
                    />
                  }
                />
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    )
  }

  if (loading) return <LoadingComponent content="Loading Enrollments..." />

  return (
    <Segment>
      <Breadcrumb size="small">
        <Breadcrumb.Section>Enrollment</Breadcrumb.Section>
        <Breadcrumb.Divider icon="right chevron" />
        <Breadcrumb.Section active>Enrollment List</Breadcrumb.Section>
      </Breadcrumb>
      <Divider horizontal>
        <Header as="h4">
          <Icon name="list alternate outline" />
          Enrollments
        </Header>
      </Divider>
      <Container>
        <Grid columns="3">
          <Grid.Column width="3" />
          <Grid.Column width="10">{enrollmentsList}</Grid.Column>
          <Grid.Column width="3" />
        </Grid>
      </Container>
    </Segment>
  )
}

Enrollments.propTypes = {
  history: PropTypes.object.isRequired,
}

export default Enrollments
