package com.mitocode;

import com.mitocode.handler.CourseHandler;
import com.mitocode.handler.EnrollmentHandler;
import com.mitocode.handler.StudentHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.PUT;
import static org.springframework.web.reactive.function.server.RequestPredicates.DELETE;

@Configuration
public class RouterConfig {

    private static final String STUDENT_PATH = "/v2/students";
    private static final String STUDENT_PATH_ID = "/v2/students/{id}";
    private static final String COURSE_PATH = "/v2/courses";
    private static final String COURSE_PATH_ID = "/v2/courses/{id}";
    private static final String ENROLLMENT_PATH = "/v2/enrollment";
    private static final String ENROLLMENT_PATH_ID = "/v2/enrollment/{id}";

    @Bean
    public RouterFunction<ServerResponse> routerFunctionStudent(StudentHandler handler) {
        return route(GET(STUDENT_PATH), handler::listAll)
                .andRoute(GET("/v2/students/list-students"), handler::listAllStudentByAge)
                .andRoute(GET(STUDENT_PATH_ID), handler::listById)
                .andRoute(POST(STUDENT_PATH), handler::register)
                .andRoute(PUT(STUDENT_PATH), handler::update)
                .andRoute(DELETE(STUDENT_PATH_ID), handler::deleteById)
                ;
    }

    @Bean
    public RouterFunction<ServerResponse> routerFunctionCourse(CourseHandler handler) {
        return route(GET(COURSE_PATH), handler::listAll)
                .andRoute(GET(COURSE_PATH_ID), handler::listById)
                .andRoute(POST(COURSE_PATH), handler::register)
                .andRoute(PUT(COURSE_PATH), handler::update)
                .andRoute(DELETE(COURSE_PATH_ID), handler::deleteById)
                ;
    }

    @Bean
    public RouterFunction<ServerResponse> routerFunctionEnrollment(EnrollmentHandler handler) {
        return route(GET(ENROLLMENT_PATH), handler::listAll)
                .andRoute(GET(ENROLLMENT_PATH_ID), handler::listById)
                .andRoute(POST(ENROLLMENT_PATH), handler::register)
                .andRoute(PUT(ENROLLMENT_PATH), handler::update)
                .andRoute(DELETE(ENROLLMENT_PATH_ID), handler::deleteById)
                ;
    }
}
