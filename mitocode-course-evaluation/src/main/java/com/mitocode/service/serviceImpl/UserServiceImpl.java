package com.mitocode.service.serviceImpl;

import com.mitocode.model.User;
import com.mitocode.repository.IGenericRepository;
import com.mitocode.repository.IRolRepository;
import com.mitocode.repository.IUserRepository;
import com.mitocode.security.UserDetailsService;
import com.mitocode.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl extends CRUDIMPL<User, String> implements IUserService {

    @Autowired
    private IUserRepository repository;

    @Autowired
    private IRolRepository rolRepository;

    @Override
    protected IGenericRepository<User, String> getRepo() {
        return repository;
    }

    @Override
    public Mono<UserDetailsService> listByUser(String userDetails) {
        Mono<User> monoUser = repository.findOneByUser(userDetails);

        List<String> roles = new ArrayList<String>();

        return monoUser.flatMap(u -> {
            return Flux.fromIterable(u.getRoles())
                    .flatMap(rol -> {
                        return rolRepository.findById(rol.getId())
                                .map(r -> {
                                    roles.add(r.getName());
                                    return r;
                                });
                    }).collectList().flatMap(list -> {
                        u.setRoles(list);
                        return Mono.just(u);
                    });
        })
        .flatMap(u -> {
            return Mono.just(new UserDetailsService(u.getUser(), u.getPassword(), u.getState(), roles));
        });
    }
}
