package com.mitocode.service.serviceImpl;

import com.mitocode.model.Course;
import com.mitocode.repository.ICourseRepository;
import com.mitocode.repository.IGenericRepository;
import com.mitocode.service.ICourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl extends CRUDIMPL<Course, String> implements ICourseService {

    @Autowired
    private ICourseRepository repository;

    @Override
    protected IGenericRepository<Course, String> getRepo() {
        return repository;
    }
}
