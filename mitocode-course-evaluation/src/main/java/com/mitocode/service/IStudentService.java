package com.mitocode.service;

import com.mitocode.model.Student;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IStudentService extends ICRUD<Student, String> {
    Flux<Student> getAllByDate();
}
