package com.mitocode.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document("roles")
public class Rol {

    @Id
    private String id;
    private String name;
}
