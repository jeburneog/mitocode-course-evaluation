import baseApi from './baseApi'
import { ENROLLMENT_ENDPOINT } from '../core/appConstants'

class EnrollmentService {
  static fetchEnrollments = () => baseApi.get(ENROLLMENT_ENDPOINT)

  static fetchEnrollmentById = (id) => baseApi.get(`${ENROLLMENT_ENDPOINT}/${id}`)

  static createEnrollment = (enrollment) => baseApi.post(ENROLLMENT_ENDPOINT, enrollment)
}

export default EnrollmentService
