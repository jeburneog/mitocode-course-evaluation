package com.mitocode.service.serviceImpl;


import com.mitocode.repository.IGenericRepository;
import com.mitocode.service.ICRUD;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public abstract class CRUDIMPL<T, ID> implements ICRUD<T, ID> {

    protected abstract IGenericRepository<T, ID> getRepo();

    @Override
    public Mono<T> register(T t) {
        return getRepo().save(t);
    }

    @Override
    public Mono<T> modify(T t) {
        return getRepo().save(t);
    }

    @Override
    public Flux<T> listAll() {
        return getRepo().findAll();
    }

    @Override
    public Mono<T> listById(ID id) {
        return getRepo().findById(id);
    }

    @Override
    public Mono<Void> delete(ID id) {
        return getRepo().deleteById(id);
    }
}
