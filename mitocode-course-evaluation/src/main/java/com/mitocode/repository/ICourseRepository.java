package com.mitocode.repository;

import com.mitocode.model.Course;

public interface ICourseRepository extends IGenericRepository<Course, String> {
}
