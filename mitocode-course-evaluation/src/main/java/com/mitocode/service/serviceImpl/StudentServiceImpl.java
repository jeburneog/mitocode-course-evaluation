package com.mitocode.service.serviceImpl;

import com.mitocode.model.Student;
import com.mitocode.repository.IGenericRepository;
import com.mitocode.repository.IStudentRepository;
import com.mitocode.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class StudentServiceImpl extends CRUDIMPL<Student, String> implements IStudentService {

    @Autowired
    private IStudentRepository repository;

    @Override
    protected IGenericRepository<Student, String> getRepo() {
        return repository;
    }

    @Override
    public Flux<Student> getAllByDate() {
        Flux<Student> studentFlux = repository.findAll(Sort.by(new Sort.Order(Sort.Direction.DESC, "age")));

        return Flux.from(studentFlux);
    }

}
