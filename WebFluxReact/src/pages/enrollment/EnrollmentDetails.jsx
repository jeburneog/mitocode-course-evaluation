import React, { useCallback, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import PropTypes from 'prop-types'
import { Breadcrumb, Container, Divider, Grid, Header, Icon, Label, Segment, Table } from 'semantic-ui-react'

import StudentService from '../../app/api/studentService'
import CourseService from '../../app/api/courseService'
import EnrollmentService from '../../app/api/enrollmentService'
import LoadingComponent from '../../components/common/LoadingComponent'

const EnrollmentDetails = ({ match }) => {
  const [enrollment, setEnrollment] = useState(null)
  const [loading, setLoading] = useState(false)

  const fetchEnrollmentById = useCallback(async () => {
    setLoading(true)
    try {
      const enrollmentReq = await EnrollmentService.fetchEnrollmentById(match.params.id)
      
      if (enrollmentReq) {
        const student = await StudentService.fetchStudent(enrollmentReq.student.id)

        const items = []
        if (enrollmentReq.items.length > 0) {
          enrollmentReq.items.forEach((item) => {
            CourseService.fetchCourse(item.course.id)
              .then((response) => {
                if (response) {
                  const courseItem = {
                    quantity: item.quantity,
                    name: response.name,
                    acronym: response.acronym,
                    id: response.id,
                    
                  }
                  items.push(courseItem)
                }

                const enrollmentDetail = {
                  id: enrollmentReq.id,
                  student,
                  items,
                  createIn: new Date(enrollmentReq.createIn).toLocaleDateString(),
                  
                }
                setEnrollment(enrollmentDetail)
              })
              .catch((error) => toast.error(error))
          })
        }
      }
      setLoading(false)
    } catch (error) {
      setLoading(false)
      toast.error(error)
    }
  }, [match.params.id])

  useEffect(() => {
    fetchEnrollmentById()
  }, [fetchEnrollmentById])

  if (loading) return <LoadingComponent content="Loading Enrollment Details..." />
  let enrollmentDetailedArea = <h4>Enrollment Details</h4>

  if (enrollment) {
    enrollmentDetailedArea = (
      <Segment.Group>
        <Segment>
          <Header as="h4" block color="violet">
            Student
          </Header>
        </Segment>
        <Segment.Group>
          <Segment>
            <p>
              <strong>Name: </strong>
              {`${enrollment.student.names} ${enrollment.student.lastNames}`}
            </p>

          </Segment>
        </Segment.Group>
        <Segment>
          <Header as="h4" block color="violet">
          Enrollment
          </Header>
        </Segment>
        <Segment.Group>
          <Segment>
            <p>
              <strong>Enrollment Code: </strong>
              {enrollment.id}
            </p>
            <p>
              <strong>Created At: </strong>
              {enrollment.createIn}
            </p>
          </Segment>
        </Segment.Group>
        <Segment>
          <Table celled striped color="violet">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell colSpan="4">
                  <Icon name="student" />
                  Courses
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Name</Table.HeaderCell>
                <Table.HeaderCell>Quantity</Table.HeaderCell>
                {/* <Table.HeaderCell>Total (/S)</Table.HeaderCell> */}
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {enrollment.items.length > 0 &&
                enrollment.items.map((item) => (
                  <Table.Row key={item.id}>
                    <Table.Cell>{item.name}</Table.Cell>
                    <Table.Cell>{item.quantity}</Table.Cell>
                    {/* {console.log(item)}
                    {console.log(enrollment)} */}
                  </Table.Row>
                ))}
            </Table.Body>
          </Table>
        </Segment>
      </Segment.Group>
    )
  }

  return (
    <Segment>
      <Breadcrumb size="small">
        <Breadcrumb.Section>Enrollment</Breadcrumb.Section>
        <Breadcrumb.Divider icon="right chevron" />
        <Breadcrumb.Section as={Link} to="/enrollment">
          Enrollment List
        </Breadcrumb.Section>
        <Breadcrumb.Divider icon="right chevron" />
        <Breadcrumb.Section active>Enrollment Detail</Breadcrumb.Section>
      </Breadcrumb>
      <Divider horizontal>
        <Header as="h4">
          <Icon name="address card outline" />
          Enrollment Detail
        </Header>
      </Divider>
      <Container>
        <Grid columns="3">
          <Grid.Column width="3" />
          <Grid.Column width="10">{enrollmentDetailedArea}</Grid.Column>
          <Grid.Column width="3" />
        </Grid>
      </Container>
    </Segment>
  )
}

EnrollmentDetails.propTypes = {
  match: PropTypes.object.isRequired,
}

export default EnrollmentDetails
