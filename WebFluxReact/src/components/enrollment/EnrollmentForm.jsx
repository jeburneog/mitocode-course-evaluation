import React, { useEffect, useState } from 'react'
import { Form as FinalForm, Field } from 'react-final-form'
import { combineValidators, isRequired } from 'revalidate'
import { Button, Form, Grid, Header, Popup, Table } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { toast } from 'react-toastify'
import PropTypes from 'prop-types'
import history from '../..'
import ErrorMessage from '../form/ErrorMessage'
import TextAreaInput from '../form/TextAreaInput'
import SelectedInput from '../form/SelectedInput'

import { fetchCourses } from '../../app/store/actions/courseActions'
import EnrollmentService from '../../app/api/enrollmentService'
import useFetchStudents from '../../app/hooks/useFetchStudents'

const validate = combineValidators({
  // student: isRequired(''),
  // courses: isRequired(''),
})

const actions = {
  fetchCourses,
}

const mapState = (state) => {
  const courses = []

  if (state.course.courses && state.course.courses.length > 0) {
    state.course.courses.forEach((item) => {
      const course = {
        key: item.id,
        text: item.name,
        value: item.id,
      }
      courses.push(course)
    })
  }

  return {
    loading: state.course.loadingCourses,
    courses,
  }
}

const EnrollmentForm = ({ fetchCourses, courses, loading }) => {
  const [students] = useFetchStudents()
  const [studentsList, setStudentsList] = useState([])
  const [loadingStudents, setLoadingStudents] = useState(true)
  const [items, setItems] = useState([])
  const [item, setItem] = useState(null)

  useEffect(() => {
    if (courses.length === 0) {
      fetchCourses()
    }
    setLoadingStudents(true)
    if (students) {
      const studentsList = []
      students.forEach((item) => {
        const student = {
          key: item.id,
          text: `${item.names} ${item.lastNames}`,
          value: item.id,
        }
        studentsList.push(student)
      })
      setStudentsList(studentsList)
      setLoadingStudents(false)
    }
  }, [students, courses.length, fetchCourses])

  const handleAddingItems = () => {
    const newItems = [...items]
    const coursesList = [...courses]
    const index = newItems.findIndex((a) => a.id === item)
    if (index > -1) {
      newItems[index] = {
        id: newItems[index].id,
        name: newItems[index].name,
        quantity: newItems[index].quantity + 1,
      }
      setItems(newItems)
    } else {
      const newItem = {
        id: item,
        name: coursesList.filter((a) => a.key === item)[0].text,
        quantity: 1,
      }
      newItems.push(newItem)
    }
    setItems(newItems)
  }

  const handleRemoveItems = (id) => {
    let updatedItems = [...items]
    updatedItems = updatedItems.filter((a) => a.id !== id)
    setItems(updatedItems)
  }

  const handleAddNewEnrollment = async (values) => {
    const newItems = [...items]
    const itemsForEnrollment = newItems.map((item) => {
      return { quantity: item.quantity, course: { id: item.id } }
    })

    const newEnrollment = {
      student: {
        id: values.student,
      },
      items: itemsForEnrollment,
    }
    try {
      const enrollment = await EnrollmentService.createEnrollment(newEnrollment)
      toast.info('The enrollment was sucessfully created')
      history.push(`enrollment/${enrollment.id}`)
    } catch (error) {
      toast.error(error)
    }
  }

  return (
    <FinalForm
      onSubmit={(values) => handleAddNewEnrollment(values)}
      validate={validate}
      render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
        <Form onSubmit={handleSubmit} error loading={loading || loadingStudents}>
          <Header as="h2" content="Add New Enrollment" color="pink" textAlign="center" />
          <Field
            name="student"
            component={SelectedInput}
            placeholder="Select a student"
            options={studentsList}
            width="1"
          />
          <Grid columns="2">
            <Grid.Row columns="2">
              <Grid.Column width="5">
                <Field
                  name="courses"
                  component={SelectedInput}
                  placeholder="Select a Courses"
                  options={courses}
                  width="1"
                  handleOnChange={(e) => setItem(e)}
                />
              </Grid.Column>
              <Grid.Column>
                <Popup
                  inverted
                  content="Add Course To Enrollment"
                  trigger={
                    <Button
                      type="button"
                      loading={submitting}
                      color="violet"
                      icon="plus circle"
                      onClick={handleAddingItems}
                      disabled={!item}
                    />
                  }
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              {items && items.length > 0 && (
                <Table celled collapsing style={{ marginLeft: '2%' }}>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>Enrollment</Table.HeaderCell>
                      <Table.HeaderCell>Quantity</Table.HeaderCell>
                      <Table.HeaderCell />
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    {items.map((item) => (
                      <Table.Row key={item.id}>
                        <Table.Cell>{item.name}</Table.Cell>
                        <Table.Cell textAlign="center">{item.quantity}</Table.Cell>
                        <Table.Cell>
                          <Popup
                            inverted
                            content="Remove from Enrollment"
                            trigger={
                              <Button
                                color="red"
                                icon="remove circle"
                                type="button"
                                onClick={() => handleRemoveItems(item.id)}
                              />
                            }
                          />
                        </Table.Cell>
                      </Table.Row>
                    ))}
                  </Table.Body>
                </Table>
              )}
            </Grid.Row>
          </Grid>
          <br />
          {submitError && !dirtySinceLastSubmit && <ErrorMessage error={submitError} text="Invalid Values" />}
          <Button
            fluid
            disabled={(invalid && !dirtySinceLastSubmit) || pristine || items.length === 0}
            loading={submitting}
            color="violet"
            content="Add New Enrollment"
          />
        </Form>
      )}
    />
  )
}

EnrollmentForm.propTypes = {
  fetchCourses: PropTypes.func.isRequired,
  courses: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
}

export default connect(mapState, actions)(EnrollmentForm)
