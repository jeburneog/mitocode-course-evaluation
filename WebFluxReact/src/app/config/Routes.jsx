import React from 'react'
import { Route, Switch, withRouter } from 'react-router-dom'
import { Container } from 'semantic-ui-react'
import PropTypes from 'prop-types'

import NotFound from '../../components/common/NotFound'
import Navbar from '../../components/navbar/Navbar'
import Students from '../../pages/students/Students'
import Courses from '../../pages/courses/Courses'
import HomePage from '../../pages/home/HomePage'
import Enrollment from '../../pages/enrollment/Enrollment'
import Enrollments from '../../pages/enrollment/Enrollments'
import EnrollmentDetails from '../../pages/enrollment/EnrollmentDetails'

const Routes = ({ authenticated }) => {
  return (
    <>
      <Route exact path="/" component={HomePage} />
      {authenticated && (
        <Route
          path="/(.+)"
          render={() => (
            <>
              <Navbar />
              <Container style={{ marginTop: '7em' }}>
                <Switch>
                  <Route exact path="/" component={HomePage} />
                  <Route path="/students" component={Students} />
                  <Route path="/courses" component={Courses} />
                  <Route path="/newEnrollment" component={Enrollment} />
                  <Route path="/enrollment/:id" component={EnrollmentDetails} />
                  <Route path="/enrollment" component={Enrollments} />
                  <Route component={NotFound} />
                </Switch>
              </Container>
            </>
          )}
        />
      )}
    </>
  )
}

Routes.propTypes = {
  authenticated: PropTypes.bool,
}

Routes.defaultProps = {
  authenticated: false,
}

export default withRouter(Routes)
