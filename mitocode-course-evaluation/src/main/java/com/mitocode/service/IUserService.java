package com.mitocode.service;

import com.mitocode.model.User;
import com.mitocode.security.UserDetailsService;
import reactor.core.publisher.Mono;

public interface IUserService extends ICRUD<User, String>{

    Mono<UserDetailsService> listByUser(String user);
}
