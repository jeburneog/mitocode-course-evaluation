import React, { useEffect, useState } from 'react'
import { combineValidators, isRequired } from 'revalidate'
import { Form as FinalForm, Field } from 'react-final-form'
import { Button, Form, Header } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import useFetchStudent from '../../app/hooks/useFetchStudent'
import ErrorMessage from '../form/ErrorMessage'
import TextInput from '../form/TextInput'

const validate = combineValidators({
  names: isRequired({ message: 'The name is required' }),
  lastNames: isRequired({ message: 'The lastName is required' }),
  dni: isRequired({ message: 'The dni is required' }),
  age: isRequired({ message: 'The age is required' }),
})

const StudentIdForm = ({ studentId, submitHandler }) => {
  const [actionLabel, setActionLabel] = useState('Add Student')
  // Custom hook
  const [student, loading] = useFetchStudent(studentId)

  useEffect(() => {
    if (studentId) {
      setActionLabel('Edit Student')
    } else setActionLabel('Add Student')
  }, [studentId])

  return (
    <FinalForm
      onSubmit={(values) => submitHandler(values)}
      initialValues={studentId && student}
      validate={validate}
      render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
        <Form onSubmit={handleSubmit} error loading={loading}>
          <Header as="h2" content={actionLabel} color="pink" textAlign="center" />
          <Field name="names" component={TextInput} placeholder="Type the Name" />
          <Field name="lastNames" component={TextInput} placeholder="Type the Lastname" />
          <Field name="dni" component={TextInput} placeholder="Type the dni" />
          <Field name="age" component={TextInput} type="number" placeholder="Type the ages" />
          {submitError && !dirtySinceLastSubmit && (
            <ErrorMessage error={submitError} text="Invalid username or password" />
          )}
          <Button
            fluid
            disabled={(invalid && !dirtySinceLastSubmit) || pristine}
            loading={submitting}
            color="violet"
            content={actionLabel}
          />
        </Form>
      )}
    />
  )
}

StudentIdForm.propTypes = {
  studentId: PropTypes.string,
  submitHandler: PropTypes.func.isRequired,
}

StudentIdForm.defaultProps = {
  studentId: null,
}

export default StudentIdForm
