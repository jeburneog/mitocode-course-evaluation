package com.mitocode.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ItemCourse {
    private Integer quantity;
    private Course course;
}
