package com.mitocode.repository;

import com.mitocode.model.Student;

public interface IStudentRepository extends IGenericRepository<Student, String> {

}
