package com.mitocode.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "students")
public class Student {

    @Id
    private String id;

    @Size(min = 3)
    @NotEmpty
    @Field("names")
    private String names;

    @Size(min = 3)
    @NotEmpty
    @Field("last_names")
    private String lastNames;

    @Size(min = 3)
    @NotEmpty
    @Field("dni")
    private String dni;

    @NotNull
    @Field("age")
    private int age;

}
