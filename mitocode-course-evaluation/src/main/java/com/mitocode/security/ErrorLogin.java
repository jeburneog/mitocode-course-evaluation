package com.mitocode.security;

import java.util.Date;

public class ErrorLogin {

    private String msg;
    private Date timeStamp;

    public ErrorLogin(String msg, Date timeStamp) {
        this.msg = msg;
        this.timeStamp = timeStamp;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
