package com.mitocode.service.serviceImpl;

import com.mitocode.model.Enrollment;
import com.mitocode.repository.IEnrollmentRepository;
import com.mitocode.repository.IGenericRepository;
import com.mitocode.service.IEnrollmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnrollmentServiceImpl extends CRUDIMPL<Enrollment, String> implements IEnrollmentService {

    @Autowired
    private IEnrollmentRepository repository;

    @Override
    protected IGenericRepository<Enrollment, String> getRepo() {
        return repository;
    }

}
