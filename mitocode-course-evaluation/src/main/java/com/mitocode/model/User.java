package com.mitocode.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@Document("users")
public class User {

    @Id
    private String id;
    private String user;
    private String password;
    private Boolean state;
    private List<Rol> roles;

}
