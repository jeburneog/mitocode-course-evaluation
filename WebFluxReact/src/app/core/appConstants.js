// keys
export const TOKEN_KEY = 'token'

// endpoints
export const STUDENT_ENDPOINT = '/students'
export const AUTH_ENDPOINT = '/login'
export const COURSES_ENDPOINT = '/courses'
export const ENROLLMENT_ENDPOINT = '/enrollment'
