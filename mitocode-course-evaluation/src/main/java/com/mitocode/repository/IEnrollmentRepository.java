package com.mitocode.repository;

import com.mitocode.model.Enrollment;

public interface IEnrollmentRepository extends IGenericRepository<Enrollment,String>{

}
