package com.mitocode.repository;

import com.mitocode.model.Rol;

public interface IRolRepository extends IGenericRepository<Rol, String> {
}
