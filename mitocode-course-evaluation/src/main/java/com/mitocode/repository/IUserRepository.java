package com.mitocode.repository;

import com.mitocode.model.User;
import reactor.core.publisher.Mono;

public interface IUserRepository extends IGenericRepository<User, String> {

    Mono<User> findOneByUser(String user);
}
