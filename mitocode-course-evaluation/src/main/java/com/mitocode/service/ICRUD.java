package com.mitocode.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICRUD<T, ID> {

    Mono<T> register(T t);
    Mono<T> modify(T t);
    Flux<T> listAll();
    Mono<T> listById(ID id);
    Mono<Void> delete(ID id);

}
