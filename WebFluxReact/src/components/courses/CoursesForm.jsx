import React, { useEffect, useState } from 'react'
import { Form, Header, Button } from 'semantic-ui-react'
import { Form as FinalForm, Field } from 'react-final-form'
import { combineValidators, composeValidators, isRequired } from 'revalidate'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import TextInput from '../form/TextInput'
import { fetchCourse, addCourse, updateCourse } from '../../app/store/actions/courseActions'
import ErrorMessage from '../form/ErrorMessage'

const validate = combineValidators({
  name: isRequired({ message: 'Please type a name' }),
  acronym: composeValidators(isRequired({ message: 'The acronym is required' }))(),
})

const actions = {
  fetchCourse,
  addCourse,
  updateCourse,
}

const mapState = (state) => ({
  course: state.course.course,
  loading: state.course.loadingCourse,
})

const CoursesForm = ({ id, course, fetchCourse, loading, addCourse, updateCourse }) => {
  const [actionLabel, setActionLabel] = useState('Add Course')

  useEffect(() => {
    if (id) {
      fetchCourse(id)
      setActionLabel('Edit Course')
    } else setActionLabel('Add Course')
  }, [fetchCourse, id])

  const handleCreateorEdit = (values) => {
    if (id) {
      updateCourse(values)
    } else {
      const newCourse = {
        name: values.name,
        acronym: values.acronym,
        state: true,
      }
      addCourse(newCourse)
    }
  }

  return (
    <FinalForm
      onSubmit={(values) => handleCreateorEdit(values)}
      initialValues={id && course}
      validate={validate}
      render={({ handleSubmit, submitting, submitError, invalid, pristine, dirtySinceLastSubmit }) => (
        <Form onSubmit={handleSubmit} error loading={loading}>
          <Header as="h2" content={actionLabel} color="pink" textAlign="center" />
          <Field name="name" component={TextInput} placeholder="Type the course name" />
          <Field name="acronym" component={TextInput} placeholder="Type the acronym of the course" />
          {submitError && !dirtySinceLastSubmit && <ErrorMessage error={submitError} text="Invalid values" />}
          <Button
            fluid
            disabled={(invalid && !dirtySinceLastSubmit) || pristine}
            loading={submitting}
            color="violet"
            content={actionLabel}
          />
        </Form>
      )}
    />
  )
}

CoursesForm.propTypes = {
  id: PropTypes.string,
  course: PropTypes.object,
  fetchCourse: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  addCourse: PropTypes.func.isRequired,
  updateCourse: PropTypes.func.isRequired,
}

CoursesForm.defaultProps = {
  id: null,
  course: null,
}

export default connect(mapState, actions)(CoursesForm)
