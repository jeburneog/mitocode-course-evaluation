package com.mitocode.handler;

import com.mitocode.model.Course;
import com.mitocode.model.Enrollment;
import com.mitocode.model.Student;
import com.mitocode.service.ICourseService;
import com.mitocode.service.IEnrollmentService;
import com.mitocode.validators.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.net.URI;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

@Component
public class EnrollmentHandler {

    @Autowired
    private IEnrollmentService service;

    @Autowired
    private RequestValidator requestValidator;

    public Mono<ServerResponse> listAll(ServerRequest request) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(service.listAll(), Student.class);
    }

    public Mono<ServerResponse> listById(ServerRequest request) {
        var id = request.pathVariable("id");
        return service.listById(id)
                .flatMap(s -> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(s))
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> register(ServerRequest serverRequest) {

        Mono<Enrollment> enrollmentMono = serverRequest.bodyToMono(Enrollment.class);

        return enrollmentMono
                .flatMap(requestValidator::validate)
                .flatMap(service::register)
                .flatMap(s -> ServerResponse.created(URI.create(serverRequest.uri().toString().concat("/").concat(s.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(s))
                );
    }

    public Mono<ServerResponse> update(ServerRequest serverRequest) {

        Mono<Enrollment> enrollmentMono = serverRequest.bodyToMono(Enrollment.class);

        return enrollmentMono
                .flatMap(requestValidator::validate)
                .flatMap(service::modify)
                .flatMap(student -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(student))
                );
    }

    public Mono<ServerResponse> deleteById(ServerRequest serverRequest) {
        var id = serverRequest.pathVariable("id");

        return service.listById(id)
                .flatMap(s -> service.delete(s.getId()).
                        then(ServerResponse.noContent().build())
                )
                .switchIfEmpty(ServerResponse.notFound().build());
    }
}
