package com.mitocode.controller;

import com.mitocode.model.Course;
import com.mitocode.model.Student;
import com.mitocode.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private IStudentService service;

    @GetMapping
    public Mono<ResponseEntity<Flux<Student>>> list() {
        Flux<Student> studentFlux = service.listAll();

        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(studentFlux)
        );
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Student>> listById(@PathVariable("id") String id) {
        return service.listById(id)
                .map(s -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(s)
                )
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping
    public Mono<ResponseEntity<Student>> register(@Valid @RequestBody Student student, final ServerHttpRequest request) {
        return service.register(student)
                .map(s -> ResponseEntity.created(URI.create(request.getURI().toString().concat("/").concat(s.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(s)
                );
    }

    @GetMapping("/list-students")
    public Mono<ResponseEntity<Flux<Student>>> listByAge() {
        Flux<Student> studentFlux = service.getAllByDate();

        return Mono.just(ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(studentFlux)
        );
    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> delete(@PathVariable("id") String id) {
        return service.listById(id)
                .flatMap(p -> service.delete(p.getId())
                        .then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT))))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping("/{id}")
    public Mono<ResponseEntity<Student>> modify(@Valid @RequestBody Student student) {
        return service.modify(student)
                .map(p -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(p)
                );
    }
}
